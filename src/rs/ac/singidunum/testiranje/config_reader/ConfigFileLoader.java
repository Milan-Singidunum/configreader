package rs.ac.singidunum.testiranje.config_reader;

import rs.ac.singidunum.testiranje.config_reader.exceptions.FileNotReadableException;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ConfigFileLoader {
    final private Map<String, String> properties;
    final private File file;

    /**
     * Attempts to extract parameters and their values from the file.<br>
     * It reads only those properties whose lines match the required format and which have not been commented out.<br>
     * The valid format for the property name is a combination of upper and lower case letter from A to Z,<br>
     * digits and the symbol underscore (underline), but the name must start with a letter.<br>
     * The property name is followed by a combination of a colon and equals signs (:=) followed by the value.<br>
     * The value can be encapsulated in quotation marks (double quotes). If the values is not quoted, it is trimmed.<br>
     * <br>Examples of valid property lines include:<br>
     * <pre style="border:1px solid #000;">
     * database_name := some_name
     * USER_PREFIX := some prefix
     * SomeStrangeProperty := "Which has spaces after the full value       like this...       "</pre>
     * Comments are lines of text starting with a dash (#). The comment can start after the value as well, for example:<br>
     * <pre style="border:1px solid #000;">
     * ThisIsAValidProperty := "That has a value" # And this is a comment
     * # This is also a comment</pre>
     * Blank or invalid lines are completely ignored.<br><br>
     * <b>See the usage example below</b><br>
     * Having a test.cfg file with this content:<br>
     * <pre style="border:1px solid #000;">
     * DB_NAME :=   test
     * DB_USER := root
     * DB_PASS :=   what_ever that has spaces!
     * DB_HOST := localhost # ovo je komentar!
     *
     * # Nista posebno, jer nema nikakvu vrednost...
     *
     * APP_NAME := Testiranje softvera
     *
     * Nije konfiguracija, ali nije ni komentar, nego ce biti ignorisano skroz...</pre>
     * A possible use case is:<br>
     * <pre style="border:1px solid #000;">
     * try {
     *     ConfigFileLoader loader = new ConfigFileLoader(new File("test.cfg"));
     *
     *     System.out.println(loader.getPropertyCount());
     *
     *     System.out.printf(loader.getProperty("DB_PASS"));
     * } catch (Exception e) {
     *     e.printStackTrace();
     * }</pre>
     *
     * @param configurationFile The file containing the configuration data
     * @throws FileNotFoundException Thrown when the file cannot be found or when it is not a file
     * @throws FileNotReadableException Throws then the file is not readable
     */
    public ConfigFileLoader(File configurationFile) throws FileNotFoundException, FileNotReadableException {
        file = configurationFile;
        properties = new HashMap<>();
        loadConfigurationData();
    }

    /**
     * Use this method to determine if the specified property has been loaded and is available.
     *
     * @param propertyName The name of the property to check for
     * @return Returns true if the specified property is available. Otherwise, returns false.
     */
    public boolean isPropertyAvailable(String propertyName) {
        return properties.keySet().contains(propertyName);
    }

    /**
     * Returns the set of strings representing the list of available names of (loaded) properties
     *
     * @return The set of strings representing the list of available names of (loaded) properties
     */
    public Set<String> getAvailableProperties() {
        return properties.keySet();
    }

    /**
     * Returns the value of the specified property as loaded from the configuration file
     *
     * @param propertyName The property name whose value is requested
     * @return The value of the specified property. If the property does not exist, it returns an empty string.
     */
    public String getProperty(String propertyName) {
        if (!isPropertyAvailable(propertyName)) {
            return "";
        }

        return properties.get(propertyName);
    }

    /**
     * Performs required checks to determine if the file can be used.
     *
     * @param file The file that is supposed to be used
     * @throws FileNotFoundException Thrown if the file does not exist, or if the name corresponds to a directory and not a file
     * @throws FileNotReadableException Thrown if the file is not readable
     */
    private void performFileChecks(File file) throws FileNotFoundException, FileNotReadableException {
        if (!file.exists()) {
            throw new FileNotFoundException("The requested configuration file does not exist.");
        }

        if (!file.canRead()) {
            throw new FileNotReadableException("The requested file cannot be read from.");
        }

        if (file.isDirectory()) {
            throw new FileNotFoundException("The requested configuration file does not exist.");
        }
    }

    /**
     * Attempts to extract parameters and their values from the file.<br>
     * It reads only those properties whose lines match the required format and which have not been commented out.<br>
     * The valid format for the property name is a combination of upper and lower case letter from A to Z,<br>
     * digits and the symbol underscore (underline), but the name must start with a letter.<br>
     * The property name is followed by a combination of a colon and equals signs (:=) followed by the value.<br>
     * The value can be encapsulated in quotation marks (double quotes). If the values is not quoted, it is trimmed.<br>
     * Examples of valid property lines include:<br>
     * <pre>
     *     database_name := some_name
     *     USER_PREFIX := some prefix
     *     SomeStrangeProperty := "Which has spaces after the full value       like this...       "
     * </pre><br>
     * Comments are lines of text starting with a dash (#). The comment can start after the value as well, for example:<br>
     * <pre>
     *     ThisIsAValidProperty := "That has a value" # And this is a comment
     *     # This is also a comment
     * </pre><br>
     * Blank or invalid lines are completely ignored.
     *
     * @throws FileNotFoundException Thrown if the file does not exist, or if the name corresponds to a directory and not a file
     * @throws FileNotReadableException Thrown if the file is not readable
     */
    private void loadConfigurationData() throws FileNotFoundException, FileNotReadableException {
        performFileChecks(file);

        Scanner scanner = new Scanner(file);

        Pattern keyValueExtractionPattern = Pattern.compile("^\\s*([A-z][A-z0-9_]*)\\s*:=\\s*\\\"?([^\\n]*?)\\\"?\\s*(?:#+.*)?$");

        properties.clear();

        while (scanner.hasNextLine()) {
            String line = scanner.nextLine().trim();

            if (line.length() == 0) {
                continue;
            }

            if (line.charAt(0) == '#') {
                continue;
            }

            Matcher lineMatcher = keyValueExtractionPattern.matcher(line);

            if (!lineMatcher.matches()) {
                continue;
            }

            String propertyName = lineMatcher.group(1);
            String propertyValue = lineMatcher.group(2);

            properties.put(propertyName, propertyValue);
        }
    }

    /**
     * Returns the count of all read properties
     * @return The count of all read properties
     */
    public int getPropertyCount() {
        return properties.size();
    }
}
