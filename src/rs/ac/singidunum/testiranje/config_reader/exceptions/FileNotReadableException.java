package rs.ac.singidunum.testiranje.config_reader.exceptions;

import java.io.IOException;

public class FileNotReadableException extends IOException {
    public FileNotReadableException() {

    }

    public FileNotReadableException(String message) {
        super(message);
    }

    public FileNotReadableException(String message, Throwable cause) {
        super(message, cause);
    }

    public FileNotReadableException(Throwable cause) {
        super(cause);
    }
}
